package nio;

/**
 * Created by artofmainstreams on 22.08.2016.
 */
public class n1_Buffers {
    /* classes
    - ByteBuffer
    - CharBuffer
    - DoubleBuffer
    - FloatBuffer
    - IntBuffer
    - LongBuffer
    - MappedByteBuffer                  /- subclass ByteBuffer -/
    - ShortBuffer
     */
    /* methods
    - abstract Object array()           /- returns a reference to the array (if supported) -/
    - abstract int arrayOffset()        /- returns the index of the first element (if supported) -/
    - final int capacity()              /- вместимость -/
    - final Buffer clear()              /- очищает буффер и возвращает ссылку на него -/
    - final Buffer flip()               /- устанавливает limit на текущую позицию и обнуляет её -/
    - abstract boolean hasArray()       /- returns true if the invoking buffers is backed by a read|write array -/
    - final boolean hasRemaining()      /- true, если остались элементы в буфере -/
    - abstract boolean isDirect()       /- true, если операции ввода/вывода выполняются непосредственно над ним -/
    - abstract boolean isReadOnly()     /- true, если read-only -/
    - final int limit()                 /- returns the invoking buffer's limit -/
    - final Buffer limit(int n)         /- устанавливает limit -/
    - final Buffer mark()               /- устанавливает mark -/
    - final int position()              /- returns the current position -/
    - final Buffer position(int n)      /- устанавливает current position -/
    - int remaining                     /- количество элементов, доступных до limit (limit-current position) -/
    - final Buffer reset()              /- current position сбрасывает до mark -/
    - final Buffer rewind()             /- устанавливает позицию на 0 -/
     */
}
