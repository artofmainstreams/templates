package nio.using.stream_based;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by artofmainstreams on 12.10.2016.
 */
public class n4_FilteredDirectory {
    /* 1-й способ фильтрации:
    - static DirectoryStream<Path> newDirectoryStream(Path dirPath, String wildcard)
    > **            /- совпадает нисколько или более символов среди директорий -/
    > [chars]       /- совпадает любой один символ, через дефис можно указать диапазон, * и ? обычные символы -/
    > [globlist]    /- совпадает с любой из массок, задаваемых в globlist -/
    Например, Files.newDirectoryStream(Paths.get(dirname), {n1, n2}*.{java,class}")
     */
    /* 2-й способ фильтрации
    - static DirectoryStream<Path> newDirectoryStream(Path dirPath, DirectoryStream.Filter<? super Path> filter)
    > boolean accept(T entry)
     */
    public static void main(String[] args) {
        String dirname = "./src/nio/";
        DirectoryStream.Filter<Path> how = new DirectoryStream.Filter<Path>() {
            public boolean accept(Path filename) throws IOException {
                if (Files.isWritable(filename)) return true;
                return false;
            }
        };

        try (DirectoryStream<Path> dirstrm = Files.newDirectoryStream(Paths.get(dirname), how)) {
            System.out.println("Directory of " + dirname);
            for (Path entry : dirstrm) {
                BasicFileAttributes attibs = Files.readAttributes(entry, BasicFileAttributes.class);
                if (attibs.isDirectory()) {
                    System.out.print("<DIR>  ");
                } else {
                    System.out.print("<FILE> ");
                }
                System.out.println(entry.getFileName());
            }
        } catch (InvalidPathException e) {
            System.out.println("Path error: " + e);
        } catch (NotDirectoryException e) {
            System.out.println(dirname + " is not a directory: " + e);
        } catch (IOException e) {
            System.out.println("I/O error: " + e);
        }
    }
}
