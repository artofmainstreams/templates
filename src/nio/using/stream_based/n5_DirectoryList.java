package nio.using.stream_based;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by artofmainstreams on 12.10.2016.
 */
public class n5_DirectoryList {
    /*
    - static Path walkFileTree(Path root, FileVisitor<? extends Path> fv)
     */
    /* interface FileVisitor<T>
    - FileVisitResult postVisitDirectory(T dir, IOException exc)            /- вызывается после посещения dir -/
    - FileVisitResult preVisitDirectory(T dir, BasicFileAttributes attribs) /- вызывается до посещения dir -/
    - FileVisitResult visitFile(T file, BasicFileAttributes attribs)        /- вызывается когда file посещён -/
    - FileVisitResult visitFileFailed(T file, IOException exc)              /- вызывается в случае fail -/
     */
    /* FileVisitResult
    > CONTINUE
    > SKIP_SIBLINGS
    > SKIP_SUBTREE
    > TERMINATE
     */
    /* ещё есть способ смотреть директории через
    java.nio.file.WatchService
     */
    public static void main(String[] args) {
        String dirname = "./src/nio/";
        System.out.println("Directory tree starting with " + dirname + ":\n");
        try {
            Files.walkFileTree(Paths.get(dirname), new MyFileVisitor());
        } catch (IOException e) {
            System.out.println("I/O error " + e);
        }
    }
}

class MyFileVisitor extends SimpleFileVisitor<Path> {
    public FileVisitResult visitFile(Path path, BasicFileAttributes attribs) throws IOException {
        System.out.println("Путь: " + path.toAbsolutePath());
        return FileVisitResult.CONTINUE;
    }
}