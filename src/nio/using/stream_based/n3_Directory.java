package nio.using.stream_based;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by artofmainstreams on 12.10.2016.
 */
public class n3_Directory {
    /*
    - static DirectoryStream<Path> newDirectoryStream(Path dirPath)
     */
    public static void main(String[] args) {
        String dirname = "./src/nio/";
        try (DirectoryStream<Path> dirstrm = Files.newDirectoryStream(Paths.get(dirname))) {
            System.out.println("Directory of " + dirname);
            for (Path entry : dirstrm) {
                BasicFileAttributes attibs = Files.readAttributes(entry, BasicFileAttributes.class);
                if (attibs.isDirectory()) {
                    System.out.print("<DIR>  ");
                } else {
                    System.out.print("<FILE> ");
                }
                System.out.println(entry.getFileName());
            }
        } catch (InvalidPathException e) {
            System.out.println("Path error: " + e);
        } catch (NotDirectoryException e) {
            System.out.println(dirname + " is not a directory: " + e);
        } catch (IOException e) {
            System.out.println("I/O error: " + e);
        }
    }
}
