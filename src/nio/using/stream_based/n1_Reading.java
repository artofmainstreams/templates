package nio.using.stream_based;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/**
 * Created by artofmainstreams on 12.10.2016.
 */
public class n1_Reading {
    /*
    - static InputSream newInputStream(Path path, OpenOption ... how)
     */
    /* можно обернуть в любой другой поток
    new BufferedInputStream(Files.newInputStream(Paths.get("./src/nio/using/test.txt"))
     */
    public static void main(String[] args) {
        int i;
        try (InputStream fin = Files.newInputStream(Paths.get("./src/nio/using/test.txt"))) {
            do {
                i = fin.read();
                if (i != -1) System.out.print((char) i);
            } while (i != -1);
        } catch (InvalidPathException e) {
            System.out.println("Path error " + e);
        } catch (IOException e) {
            System.out.println("I/O error " + e);
        }
    }
}
