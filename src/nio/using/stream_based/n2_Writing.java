package nio.using.stream_based;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/**
 * Created by artofmainstreams on 12.10.2016.
 */
public class n2_Writing {
    /*
    - static OutputSream newOutputStream(Path path, OpenOption ... how)
     */
    /* можно обернуть в любой другой поток
    new BufferedInputStream(Files.newInputStream(Paths.get("./src/nio/using/test.txt"))
     */
    public static void main(String[] args) {
        try (OutputStream fout =
        new BufferedOutputStream(
                Files.newOutputStream(Paths.get("./src/nio/using/test.txt")))) {
            for (int i = 0; i < 26; ++i) {
                fout.write((byte) ('A' + i));
            }
        } catch (InvalidPathException e) {
            System.out.println("Path error " + e);
        } catch (IOException e) {
            System.out.println("I/O error " + e);
        }
    }
}
