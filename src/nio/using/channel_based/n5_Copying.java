package nio.using.channel_based;

import java.io.IOException;
import java.nio.file.*;

/**
 * Created by artofmainstreams on 10.10.2016.
 */
public class n5_Copying {
    /*
    - static Path copy(Path src, Path dest, CopyOption ... how)
    > StandardCopyOption.COPY_ATTRIBUTES
    > StandardLinkOption.NOFOLLOW_LINKS
    > StandardCopyOption.REPLACE_EXISTING
     */
    public static void main(String[] args) {
        try {
            Path source = Paths.get("./src/nio/using/test.txt");
            Path target = Paths.get("./src/nio/using/copyTest.txt");
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        } catch (InvalidPathException e) {
            System.out.println("Path error " + e);
        } catch (IOException e) {
            System.out.println("I/O error " + e);
        }
    }
}
