package nio.using.channel_based;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Created by artofmainstreams on 10.10.2016.
 */
public class n3_Writing {
    /*
    - static SeekableByteChannel newByteChannel(Path path, OpenOption ... how)
     */
    public static void main(String[] args) {
        try (FileChannel fChan = (FileChannel)
                Files.newByteChannel(Paths.get("./src/nio/using/test.txt"),
                        StandardOpenOption.WRITE,
                        StandardOpenOption.CREATE)) {
            ByteBuffer mBuf = ByteBuffer.allocate(26);

            for (int h = 0; h < 3; h++) {
                for (int i = 0; i < 26; i++) {
                    mBuf.put((byte) ('A' + i));
                }

                mBuf.rewind();  //перематываем буффер в начало
                fChan.write(mBuf);
                mBuf.rewind();
            }

        } catch (InvalidPathException e) {
            System.out.println("Path error " + e);
        } catch (IOException e) {
            System.out.println("I/O error " + e);
            System.exit(1);
        }
    }
}
