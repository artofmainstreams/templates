package nio.using.channel_based;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/**
 * Created by artofmainstreams on 10.10.2016.
 */
public class n2_MapReading {
    /* делаем копию файла прямо в память, буфер
    - MappedByteBuffer map(FileChannel.MapMode how, long pos, long size)
    > MapMode.READ_ONLY
    > MapMODE.READ_WRITE
    > Map.Mode.PRIVATE          /- private copy file, результат не влияет на вызываемый -/
     */
    public static void main(String[] args) {
        //создаём канал + связываем с буфером
        //FileChannel нужен для вызова метода map
        try (FileChannel fChan = (FileChannel)
                Files.newByteChannel(Paths.get("./src/nio/using/test.txt"))) {
            long fSize = fChan.size();
            System.out.println("Size: " + fSize);
            MappedByteBuffer mBuf = fChan.map(FileChannel.MapMode.READ_ONLY, 0, fSize);

            for (int i = 0; i < fSize; i++) {
                System.out.print((char) mBuf.get());
            }
            System.out.println();
        } catch (InvalidPathException e) {
            System.out.println("Path error " + e);
        } catch (IOException e) {
            System.out.println("I/O error " + e);
        }
    }
}
