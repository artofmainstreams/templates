package nio.using;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by artofmainstreams on 12.10.2016.
 */
public class n1_File {
    public static void main(String[] args) {
        Path filepath = Paths.get("./src/nio/using/test.txt");
        System.out.println("File name: " + filepath.getFileName());
        System.out.println("Path: " + filepath);
        System.out.println("Absolute path: " + filepath.toAbsolutePath());
        System.out.println("Parent: " + filepath.getParent());

        if (Files.exists(filepath)) {
            System.out.println("File exists");
        } else {
            System.out.println("File does not exist");
        }

        try {
            if (Files.isHidden(filepath)) {
                System.out.println("File is hidden");
            } else {
                System.out.println("File is not hidden");
            }

            if (Files.isWritable(filepath)) {
                System.out.println("File is writable");
            } else {
                System.out.println("File is not writable");
            }

            if (Files.isReadable(filepath)) {
                System.out.println("File is readable");
            } else {
                System.out.println("File is not readable");
            }
        } catch (IOException e) {
            System.out.println("I/O error " + e);
        }

        try {
            BasicFileAttributes attribs = Files.readAttributes(filepath, BasicFileAttributes.class);

            if (attribs.isDirectory()) {
                System.out.println("The file is a directory");
            } else {
                System.out.println("The file is not a directory");
            }

            if (attribs.isRegularFile()) {
                System.out.println("The file is a normal file");
            } else {
                System.out.println("The file is not a normal file");
            }

            if (attribs.isSymbolicLink()) {
                System.out.println("The file is a symbolic link");
            } else {
                System.out.println("The file is not a symbolic link");
            }

            System.out.println("File last modified: " + attribs.lastModifiedTime());
            System.out.println("File size: " + attribs.size() + " bytes");
        } catch (IOException e) {
            System.out.println("Error reading attributies: " + e);
        }
    }
}
