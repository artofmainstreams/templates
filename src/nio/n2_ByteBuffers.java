package nio;

/**
 * Created by artofmainstreams on 22.08.2016.
 */
public class n2_ByteBuffers {
    /* methods                                  /- для остальных классов идентичны -/
    - abstract byte get()                       /- копирует буффер в массив -/
    - ByteBuffer get(byte vals[])
    - ByteBuffer get(byte vals[],
                    int start,
                    int num)
    - abstract byte get(int idx)
    - abstract ByteBuffer put(byte b)           /- копирует b в текущую позицию -/
    - final ByteBuffer put(byte vals[])
    - ByteBuffer put(byte vals[],
                    int start,
                    int num)
    - ByteBuffer put(ByteBuffer bb)
    - abstract ByteBuffer put(int idx, byte b)
     */
}
