package nio;

/**
 * Created by artofmainstreams on 24.08.2016.
 */
public class n6_Files {
    /* methods
     - list
     - walk
     - lines
     - find
     - getFilesAttributeView
     - static Path copy(Path src, Path dest, CopyOption ... how)
     - static Path createDirectory(Path path, FileAttribute<?> ... attribs)
     - static Path createFile(Path path, FileAttribute<?> ... attribs)
     - static void delete(Path path)
     - static boolean exists(Path path, LinkOption ... opts)        /- NOFOLLOW_LINKS for not symbolics links -/
     - static boolean isDirectory(Path path, LinkOption ... opts)
     - static boolean isExecutable(Path path)
     - static boolean isHidden(Path path)
     - static boolean isReadable(Path path)
     - static boolean isRegularFile(Path path, LinkOption ... opts)
     - static boolean isWritable(Path path)
     - static Path move(Path src, Path dest, CopyOption ... how)
     - static seekableByteChannel newByteChannel(Path path, OpenOption ... how)
     - static DirectoryStream<Path> newDirectoryStream(Path path)
     - static InputSream newInputStream(Path path, OpenOption ... how)
     - static OutputStream newOutputStream(Path path, OpenOption ... how)
     - static boolean notExist(Path path, LinkOption ... opts)
     - static <A extends BasicFileAttributes> A readAttributes(Path path, Class<A> attribType, LinkOption ... opts)
     - static long size(Path path)
     */
    /* open options
    - APPEND            /- добавить в конец файла -/
    - CREATE            /- создать файл, если не существует -/
    - CREATE_NEW        /- создать только если не существует -/
    - DELETE_ON_CLOSE   /- удалить файл при закрытии -/
    - DSYNC             /- немедленно записывать данные в файл -/
    - READ              /- открыть файл для input операций -/
    - SPARSE            /- указать, что файл разбросан, а значит, не может быть полностью заполнен данными -/
    - SYNC              /- немедленно записывать данные и metadata в файл -/
    - TRUNCATE_EXISTING /- укоротить до нуля длину уже существующего открываюшегося файла -/
    - WRITE             /- открыть файл для output операций -/
     */
    /* attributes
    - FileTime creationTime()
    - object fileKey()
    - boolean isDirectory()         /- true если directory -/
    - boolean isOther()             /- true если symbolic link или directory -/
    - boolean isRegularFile()       /- true если file -/
    - boolean isSymbolicLink()      /- true если symbolic link -/
    - FileTime lastAccessTime()
    - FileTime lastModifiedTime()
    - long size()
     */
    /* attributes DOS
    - boolean isArchive()
    - boolean isHidden()
    - boolean isReadOnly()
    - boolean isSystem()
     */
    /* attributes POSIX
    - GroupPrincipal group()
    - UserPrincipal owner()
    - Set<PosixFilePermission> permission()
     */
}
