package nio;

/**
 * Created by artofmainstreams on 24.08.2016.
 */
public class n5_Path {
    /* create
    - static Path get(String pathname, String ... parts)    /- не создаёт файл -/
    - static Path get(URI uri)
     */
    /* methods
    - boolean endsWith(String path)     /- true, если оканчивается на path -/
    - boolean endsWith(Path path)
    - Path getFileName()
    - Path getName(int idx)             /- root элемент это 0, самый правый это getNameCount()-1 -/
    - int getNameCount()                /- возвращает количество элементов после root -/
    - Path getParent()
    - Path getRoot()
    - boolean isAbsolute()
    - Path resolve(Path path)           /- Если path абсолютный, return path, если относительный, добавляется
                                            к текущему, если path пустой, возвращается вызванный -/
    - Path resolve(String path)
    - boolean startsWith(String path)
    - boolean startsWith(Path path)
    - Path toAbsolutePath()             /- возвращает Path с полным путём -/
    - String toString()                 /- возвращает строковое представление Path -/
     */
}
