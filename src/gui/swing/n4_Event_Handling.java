package gui.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by artofmainstreams on 14.10.2016.
 */
public class n4_Event_Handling {
    JLabel jLabel;

    n4_Event_Handling() {
        JFrame jFrame = new JFrame("An Event Example");
        jFrame.setLayout(new FlowLayout());
        jFrame.setSize(220, 90);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JButton jButtonAlpha = new JButton("Alpha");
        JButton jButtonBeta = new JButton("Beta");
        JTextField jTextField = new JTextField(30);


        jButtonAlpha.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jButtonAlpha.setText(jTextField.getText());
            }
        });

        jButtonBeta.addActionListener(e -> jLabel.setText("Beta was pessed"));
        jTextField.addActionListener(e -> jLabel.setText(jTextField.getText()));

        jFrame.add(jButtonAlpha);
        jFrame.add(jButtonBeta);
        jFrame.add(jTextField);

        jLabel = new JLabel("Press a button");
        jFrame.add(jLabel);

        jFrame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new n4_Event_Handling();
            }
        });
    }
}
