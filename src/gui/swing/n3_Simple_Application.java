package gui.swing;

import javax.swing.*;

/**
 * Created by artofmainstreams on 14.10.2016.
 */
public class n3_Simple_Application {
    /* methods
    - void setSize(int width, int height)
    - void setDefaultCloseOperation(int what)
    > EXIT_ON_CLOSE
    > DISPOSE_ON_CLOSE
    > HIDE_ON_CLOSE
    > DO_NOTHING_ON_CLOSE
    - Component add(Component comp)
    - static void invokeLater(Runnable obj)
    - static void invokeAndWait(Runnable obj)
     */
    n3_Simple_Application() {
        //создаём окошечко с заголовком
        JFrame jFrame = new JFrame("A simple Swing Application");
        //устанавливаем размер окошечка
        jFrame.setSize(875,100);
        //при нажатии на крестик не только закрыть окно, но и завершить программу
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JLabel jLabel = new JLabel("Swing means powerful GUIs");
        jFrame.add(jLabel);
        jFrame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new n3_Simple_Application();
            }
        });
    }

}
