package networking;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by artofmainstreams on 13.10.2016.
 */
public class n4_URL {
    /* constructors
    - URL (String urlSpecifier)
    - URL (Strinf protocolName, String hostName, int port, String path)
    - URL (String protocolName, String hostName, String path)
     */
    public static void main(String[] args) throws MalformedURLException {
        URL hp = new URL("http://www.HerbSchildt.com/WhatsNew");

        System.out.println("Protocol: " + hp.getProtocol());
        System.out.println("Port: " + hp.getPort());
        System.out.println("Host: " + hp.getHost());
        System.out.println("File: " + hp.getFile());
        System.out.println("Ext: " + hp.toExternalForm());
    }
}
