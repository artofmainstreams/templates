package networking;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

/**
 * Created by artofmainstreams on 13.10.2016.
 */
public class n5_URLConnection {
    /* methods
    - URLConnection openConnection()
    - int getContentLength()
    - long getContentLengthLong()
    - String getContentType()
    - long getDate()
    - long getExpiration()
    - String getHeaderField(int idx)
    - String getHeaderField(String fieldName)
    - String getHeaderFieldKey(int idx)
    - Map <String, List <String>> getHeaderFields() /- returns a map that contains all of the header fields
                                                                                                    and values-/
    - long getLastModified()
    - InputStream getInputStream()
     */
    public static void main(String[] args) throws Exception {
        int c;
        URL hp = new URL("http://google.com");
        URLConnection hpCon = hp.openConnection();

        long date = hpCon.getDate();
        if (date == 0) {
            System.out.println("No date information");
        } else {
            System.out.println("Date: " + new Date(date));
        }

        System.out.println("Content-Type: " + hpCon.getContentType());

        date = hpCon.getExpiration();
        if (date == 0) {
            System.out.println("No expiration information");
        } else {
            System.out.println("Date: " + new Date(date));
        }

        date = hpCon.getLastModified();
        if (date == 0) {
            System.out.println("No last-modified information");
        } else {
            System.out.println("Date: " + new Date(date));
        }

        long len = hpCon.getContentLengthLong();
        if (len == -1) {
            System.out.println("Content length unavailable");
        } else {
            System.out.println("Content-Length: " + len);
        }

        if (len != 0) {
            System.out.println("=== Content ===");
            InputStream input = hpCon.getInputStream();
            while (((c = input.read()) != -1)) {
                System.out.print((char) c);
            }
            input.close();
        } else {
            System.out.println("No content available");
        }
    }
}
