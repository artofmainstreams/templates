package networking;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by artofmainstreams on 12.10.2016.
 */
public class n3_Socket {
    /* constructors
    - Socket(String hostName, int port)         /- создаёт socket, соединяющийся к указанному hostName и port -/
    - Socket(InetAddress ipAddress, int port)
     */
    /* methods
    - InetAddress getInetAddress()
    - int getPort()
    - int getLocalPort
    - InputStream getInputStream()
    - OutputStream getOutputStream()
    - connect
    - boolean isConnected()
    - boolean isBound()
    - boolean isClosed()
    - close
     */
    public static void main(String[] args) throws Exception {
        int c;
        try (Socket s = new Socket("whois.internic.net", 43)) {

            InputStream in = s.getInputStream();
            OutputStream out = s.getOutputStream();

            String str = (args.length == 0 ? "yandex.ru" : args[0]) + "\n";
            byte[] buf = str.getBytes();

            out.write(buf);

            while ((c = in.read()) != -1) {
                System.out.print((char) c);
            }
        }
    }
}
