package networking;

/**
 * Created by artofmainstreams on 12.10.2016.
 */
public class n1_java_net {
    /* classes
    - Authenticator
    - CacheRequest
    - CacheResponse
    - ContentHandler
    - CookieHandler
    - CookieManager
    - DatagramPacket
    - DatagramSocket
    - DatagramSocketImpl
    - HttpCookie
    - HttpURLConnection
    - IDN
    - Inet4Address
    - Inet6Address
    - InetAddress
    - InetSocketAddress
    - InterfaceAddress
    - JarURLConnection
    - MulticastSocket
    - NetPermission
    - NetworkInterface
    - PasswordAuthentication
    - Proxy
    - ProxySelector
    - ResponseCache
    - SecureCacheResponse
    - ServerSocket
    - Socket
    - SocketAddress
    - SocketImpl
    - SocketPermission
    - StandardSocketOption
    - URI
    - URL
    - URLClassLoader
    - URLConnection
    - URLDecoder
    - URLEncoder
    - URLPermission
    - URLStreamHandler
     */
    /* interfaces
    - ContentHandlerFactory
    - CookiePolicy
    - CookieStore
    - DatagramSocketImplFactory
    - FileNameMap
    - ProtocolFamily
    - SocketImplFactory
    - SocketOption
    - SocketOptions
    - URLStreamHandlerFactory
     */
}
