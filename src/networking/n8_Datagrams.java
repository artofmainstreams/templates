package networking;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by artofmainstreams on 13.10.2016.
 */
public class n8_Datagrams {
    /* DatagramSocket constructors
    - DatagramSocket()
    - DatagramSocket(int port)
    - DatagramSocket(int port, InetAddress ipAddress)
    - DatagramSocket(SocketAddress address)
     */
    /* DatagramSocket methods
    - InetAddress getInetAddress
    - int getLocalPort
    - int getPort()
    - boolean isBound
    - boolean isConnected
    - void setSoTimeout(int millis)
     */
    /* DatagramPacket constructors
    - DatagramPacket(byte[] data, int size)
    - DatagramPacket(byte[] data, int offset, int size)
    - DatagramPacket(byte[] data, int size, InetAddress ipAddress, int port)
    - DatagramPacket(byte[] data, int offset, int size, InetAddress ipAddress, int port)
     */
    /* DataPacket methods
    - InetAddress getAddress()
    - byte[] getData()
    - int getLength()
    - int getOffset()
    - int getPort()
    - void setAddress(InetAddress ipAddress)
    - void setData(byte[] data)
    - void setData(byte[] data, int idx, int size)
    - void setLength(int size)
    - void setPort(int port)
     */
    public static int SERVER_PORT = 998;
    public static int CLIENT_PORT = 999;
    public static int BUFFER_SIZE = 1024;
    public static DatagramSocket datagramSocket;
    public static byte[] buffer = new byte[BUFFER_SIZE];

    public static void theServer() throws Exception {
        System.out.println("server");
        int pos = 0;
        while (true) {
            int c = System.in.read();
            switch (c) {
                case -1:
                    System.out.println("Server quits");
                    datagramSocket.close();
                    return;
                case '\r':
                    break;
                case '\n':
                    datagramSocket.send(new DatagramPacket(buffer, pos, InetAddress.getLocalHost(), CLIENT_PORT));
                    pos = 0;
                    break;
                default:
                    buffer[pos++] = (byte) c;
            }

        }
    }

    public static void theClient() throws Exception {
        System.out.println("client");
        while (true) {
            DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);
            datagramSocket.receive(datagramPacket);
            System.out.println(new String(datagramPacket.getData(), 0, datagramPacket.getLength()));
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 1) {
            datagramSocket = new DatagramSocket(SERVER_PORT);
            theServer();
        } else {
            datagramSocket = new DatagramSocket(CLIENT_PORT);
            theClient();
        }
    }
}
