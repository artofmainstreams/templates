package networking;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by artofmainstreams on 12.10.2016.
 */
public class n2_InnetAddress {
    /* methods (constructors)
    - static InetAddress getLocalHost() throws UnknownHostException
    - static InetAddress getByName(String hostName) throws UnknownHostException
    - static InetAddress[] getAllByName(String hostName) throws UnknownHostException
    - getByAddress
     */
    /* methods
    - boolean equals(Object other)  /- true, если this объект имеет тот же адресс, что и other -/
    - byte[] getAddress()           /- address в виде массива -/
    - String getHostAddress()       /- возвращает host address в виде строки -/
    - String getHostName()          /- возвращает host name в виде строки -/
    - boolean isMulticastAddress()  /- возвращает true, если address is a multicast -/
    - String toString()             /- hostname и IP -/
     */

    public static void main(String[] args) throws UnknownHostException {
        InetAddress address = InetAddress.getLocalHost();
        System.out.println(address);

        address = InetAddress.getByName("www.yandex.ru");
        System.out.println(address);

        InetAddress[] SW = InetAddress.getAllByName("www.yandex.ru");
        for (int i = 0; i < SW.length; i++) {
            System.out.println(i + ": " + SW[i]);
        }

        for (byte i : address.getAddress()) {
            System.out.println(">> " + i);
        }
    }
}
