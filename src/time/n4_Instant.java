package time;

import java.time.Duration;
import java.time.Instant;

/**
 * Created by artofmainstreams on 26.08.2016.
 */
public class n4_Instant {
    /*
    Класс Instant используется для работы с машиночитаемым форматом времени — он
    сохраняет дату и время в так называемый «unix timestamp (отметку времени)»
     */
    public static void main(String[] args) {
        //Текущая отметка времени
        Instant timestamp = Instant.now();
        System.out.println("Текущая отметка времени : "+timestamp);

        //Instant для timestamp
        Instant specificTime = Instant.ofEpochMilli(timestamp.toEpochMilli());
        System.out.println("Instant для timestamp : " + specificTime);

        //Пример использования Duration
        Duration sixtyDay = Duration.ofDays(60);
        System.out.println(sixtyDay);
    }
}
