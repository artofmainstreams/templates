package exception;

import java.io.IOException;

/**
 * Created by frien on 24.07.2016.
 */
public class _7_Chained {
    public static void main(String[] args) {
        try { //chained exception
            function();
        } catch (NullPointerException e) {
            System.out.println("Caught: " + e);
            System.out.println("Original cause: " + e.getCause());
        } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) { //multi-catch
            System.out.println("Some exception");
        }
    }

    static void function() { //есть вариант через конструктор
        NullPointerException e = new NullPointerException("top layer");
        e.initCause(new ArithmeticException("cause"));
        throw e;
    }
}
