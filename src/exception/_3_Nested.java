package exception;

/**
 * Created by frien on 23.07.2016.
 */
public class _3_Nested {
    public static void main(String[] args) {
        try { //nested exceptions
            _3_Nested.function();
        } catch (ArithmeticException e) {
            System.out.print("Арифметическая ошибка: ");
            System.out.println(e); //используем переопределённый метод _2_toString_and_valueOf()
        } catch (Exception e) {
            System.out.println("Другая ошибка: ");
            System.out.println(e); //используем переопределённый метод _2_toString_and_valueOf()
        }
    }

    public static void function() {
        try {
            System.out.println("Выбрасываю ошибку");
            System.out.println(7/0); //Throwable >> Exception >> RuntimeException >> ArithmeticException
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Собственная обработка ошибок: ");
            System.out.println(e); //используем переопределённый метод _2_toString_and_valueOf()
        }
    }
}
