package exception;

/**
 * Created by frien on 23.07.2016.
 */
public class _1_DontCatch {
    public static void main(String[] args) {
        //don't catch exceptions
        //exception создаётся системой и выводится в stack trace
        System.out.println(7/0); //Throwable >> Exception >> RuntimeException >> ArithmeticException
    }
}
