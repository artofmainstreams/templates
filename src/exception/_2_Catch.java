package exception;

/**
 * Created by frien on 23.07.2016.
 */
public class _2_Catch {
    public static void main(String[] args) {
        try { //catch exceptions
            _2_Catch.function();
        } catch (ArithmeticException e) {
            System.out.print("Арифметическая ошибка: ");
            System.out.println(e); //используем переопределённый метод toString()
        } catch (Exception e) {
            System.out.println("Другая ошибка: ");
            System.out.println(e); //используем переопределённый метод toString()
        } finally {
            System.out.println("Блок, который выполняется всегда");
        }
    }

    public static void function() {
        System.out.println("Выбрасываю ошибку");
        System.out.println(7/0); //Throwable >> Exception >> RuntimeException >> ArithmeticException
    }
}
