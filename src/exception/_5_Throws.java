package exception;

/**
 * Created by frien on 23.07.2016.
 */
public class _5_Throws {
    static void function() throws IllegalAccessException { //Exception должны быть обработаны или выброшены
        System.out.println("Inside throw");
        throw new IllegalAccessException("test");
    }

    public static void main(String[] args) {
        try {
            function();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
