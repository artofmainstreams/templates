package exception;

/**
 * Created by frien on 23.07.2016.
 */
public class _6_Itself {
    public static void main(String[] args) {
        try { //собственное исключение
            compute(1);
            compute(20);
        } catch (MyException e) {
            System.out.println("Caught " + e);
        }
    }

    static void compute(int a) throws MyException {
        System.out.println("Called compute(" + a + ")");
        if (a > 10) {
            throw new MyException(a);
        }
        System.out.println("Normal exit");
    }
}

class MyException extends Exception {
    private int detail;

    MyException(int a) {
        detail = a;
    }

    @Override
    public String toString() {
        return "MyException{" + "detail=" + detail + '}';
    }
}