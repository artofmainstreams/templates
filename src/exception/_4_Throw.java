package exception;

/**
 * Created by frien on 23.07.2016.
 */
public class _4_Throw {
    public static void main(String[] args) {
        try {
            function();
        } catch (NullPointerException e) {
            System.out.println("Recaught" + e);
        }
    }

    static void function() {
        try {
            throw new NullPointerException("Создали ошибку");
        } catch (NullPointerException e) {
            System.out.println("Обработка");
            throw e; //выбрасываем
        }
    }
}
