package multithread;

import static java.lang.Thread.*;

/**
 * Created by frien on 24.07.2016.
 */
public class _1_MainThread {
    public static void main(String[] args) {
        Thread t = currentThread();
        System.out.println("Current thread: " + t);
        t.setName("My super thread");
        t.setPriority(NORM_PRIORITY); //from 1 to 10
        /*  state
        - runnable
        - blocked
        - new
        - terminated
        - timed_waiting
        - waiting
         */
        System.out.println("State: " + t.getState());
        System.out.println("After name change: " + t);

        try {
            for (int n = 5; n > 0; --n) {
                System.out.println(n);
                Thread.sleep(1000);
            }
        }catch (InterruptedException e) {
            System.out.println("Main thread interrupted");
        }

    }
}
