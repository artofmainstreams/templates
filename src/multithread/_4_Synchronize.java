package multithread;

/**
 * Created by frien on 25.07.2016.
 */
public class _4_Synchronize {
    public static void main(String[] args) {
        /* synchronized
        - synchronized method() {}
        - run() { synchronized(obj) { obj.method(); }}
         */
        CallMe target = new CallMe();
        Caller ob1 = new Caller(target, "Hello");
        Caller ob2 = new Caller(target, "Synchronized");
        Caller ob3 = new Caller(target, "World");
    }
}

class CallMe {
    synchronized void call(String msg) { //все остальные synchronized методы блокируются в том же классе
        System.out.print("[" + msg);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
        System.out.println("]");
    }

    void callNotSync(String msg) {
        System.out.print("!" + msg);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
        System.out.println("!");
    }
}


class Caller implements Runnable {
    String msg;
    CallMe target;
    Thread t;

    public Caller(CallMe target, String msg) {
        this.msg = msg;
        this.target = target;
        t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        synchronized (target) {
            target.callNotSync(msg);
        }
        target.call(msg);
    }
}
