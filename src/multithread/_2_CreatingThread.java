package multithread;

/**
 * Created by frien on 24.07.2016.
 */
public class _2_CreatingThread {
    public static void main(String[] args) {
        /* creating threads
        - implement the Runnable interface
        - extends the Thread class
        */
        new MyThreadRunnable();
        new MyThreadExtends();

        try {
            for (int n = 5; n > 0; --n) {
                System.out.println("Main Thread: " + n);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Main thread interrupted");
        }
        System.out.println("Main thread exiting");
    }
}

class MyThreadRunnable implements Runnable {
    Thread t;

    MyThreadRunnable() {
        t = new Thread(this, "Мой поток"); //first arg: Runnable, second arg: nameThread
        System.out.println("Child thread: " + t);
        t.start(); //запуск потока!
    }

    @Override
    public void run() {
        try {
            for (int n = 5; n > 0; --n) {
                System.out.println("Runnable child Thread: " + n);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Runnable child interrupted");
        }
        System.out.println("Exiting runnable child thread");
    }
}

class MyThreadExtends extends Thread {
    MyThreadExtends() {
        super("Мой поток");
        System.out.println("Child thread: " + this);
        start();
    }

    @Override
    public void run() {
        try {
            for (int n = 5; n > 0; --n) {
                System.out.println("Extends child thread: " + n);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Extends child interrupted.");
        }
        System.out.println("Exiting extends child thread.");
    }
}
