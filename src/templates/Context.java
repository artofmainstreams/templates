package templates;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by artofmainstreams on 07.06.2017.
 */

/**
 * Позволяет получать доступ только к тем сервисам, которые нужны
 * Паттерн ли это?
 */
public class Context {
    private Map<Class<?>, Object> context = new HashMap();

    public void add(Class<?> clazz, Object object) {
        if (context.containsKey(clazz)) {
            // Error
        }
        context.put(clazz, object);
    }

    public Object get(Class<?> clazz) {
        return context.get(clazz);
    }
//      тоже верное решение
//    public <T> T get(Class<T> key){
//        return (T) context.get(key);
//    }
}
