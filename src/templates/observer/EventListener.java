package templates.observer;


/**
 * Created by artofmainstreams on 07.06.2017.
 */
public interface EventListener {
    /**
     * При наступлении события вызывается этот метод
     */
    public void handle();
}
