package templates.observer;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by artofmainstreams on 07.06.2017.
 */

/**
 * Класс с событиями, при наступлении события оповещает подписчиков об этом
 */
public class EventSource {
    private List<EventListener> listeners = new LinkedList<>();

    public void addListener(EventListener listener) {
        listeners.add(listener);
    }

    public void removeListener(EventListener listener) {
        listeners.remove(listener);
    }

    /**
     * В функцию можно передать информацию о событии
     */
    public void fireEvent() {
        for(EventListener listner : listeners) {
            listner.handle();
        }
    }
}
