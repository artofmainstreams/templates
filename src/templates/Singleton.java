package templates;

/**
 * Created by artofmainstreams on 07.06.2017.
 */
public class Singleton {
    private static Singleton singleton;
    private Singleton() {}
    private String value;

    public static Singleton instance() {
        if (singleton == null) {
            singleton = new Singleton();
        }
        return singleton;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static void main(String[] args) {
        Singleton.instance().setValue("3");
        System.out.println(Singleton.instance().getValue());
    }
}
