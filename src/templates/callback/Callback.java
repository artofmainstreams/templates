package templates.callback;

/**
 * Created by artofmainstreams on 07.06.2017.
 */
public interface Callback {
    public void function(String str);
}
