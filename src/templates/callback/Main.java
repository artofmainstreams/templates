package templates.callback;

/**
 * Created by artofmainstreams on 07.06.2017.
 */
public class Main {
    public static void main(String[] args) {
        CallbackImplements a = new CallbackImplements();
        //передаём в аргументах функцию
        a.doGet(7, str -> System.out.println("Something"));
    }
}
