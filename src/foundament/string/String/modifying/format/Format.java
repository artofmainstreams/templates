package foundament.string.String.modifying.format;

import java.util.Date;

/**
 * Created by frien on 28.07.2016.
 */
public class Format {
    /* format
    - static String format(String fmtstr, Objects...args)
    - static String format(Locale loc, String fmtstr, Objects...args)
     */
    /*
    d - десятичные числа
    x - 16-ричные числа
    o - 8-ричные числа
    f - fixed-point floating-point
    e - exponential floating-point
    g - general floating-point (the shorter od e and f)
    a - 16-ричные floating-point
    s - String
    c - character
    b - boolean
    h - hash code
    tx or Tx - date and time (T forces uppercase)
    % - %
    n - the platform-dependent line separator
     */
    /* Флаги

     */
    public static void main(String[] args) {
        double x = 10000.0 / 3.0;
        String message = String.format("");
        System.out.printf("%,.2f\n", x);
        //для времени очень много разных букв
        System.out.printf("%tc\n", new Date());
        //%индекс$
        System.out.printf("%1$s %2$tB %2$te, %2$tY\n", "Текущая дата: ", new Date());
        //или так
        System.out.printf("%s %2tB %<te, %<tY\n", "Текущая дата: ", new Date());
        System.out.println(String.format("Строка %s и число: %d", "некоторая", 8));
    }
}
