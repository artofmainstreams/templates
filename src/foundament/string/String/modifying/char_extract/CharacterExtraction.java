package foundament.string.String.modifying.char_extract;

/**
 * Created by frien on 27.07.2016.
 */
public class CharacterExtraction {
    /* строка символов
    - void getChars(int sourceStart, int sourceEnd, char target[], int targetStart) /- 10...16 -/
    - byte[] getBytes()                  /- полезно когда из 16-bit Unicode in the 8-bit ASCII -/
    - byte[] getBytes(Charset charset)
    - byte[] getBytes(String charsetName)
    - char[] toCharArray
     */
    /* символ
    - char charAt(int index)
     */
    /* codePoint
    - int codePointAt(int index)                    /- Unicode указанного символа -/
    - int codePointBefore(int index)                /- Unicode предыдущего символа -/
    - intCodePointCount(int start, int end)         /- количество символов -/
     */
    /* offsetByCodePoints
    - int offsetByCodePoints(int start, int num)
     */

    public static void main(String[] args) {
        char c = "string".charAt(1); //h
        String s = "This is just a string for the getChars method";
        int start = 10;
        int end = 17;
        char[] buf = new char[end - start];
        s.getChars(start, end, buf, 0);
        System.out.println(buf);
        byte[] arrayBytes = s.getBytes();
        char[] simple = s.toCharArray();
    }
}
