package foundament.string.String.modifying.search;

/**
 * Created by frien on 27.07.2016.
 */
public class Searching {
    public static void main(String[] args) {
        /* indexOf(str)
        - int indexOf(int ch)                           /- first occurence of a character -/
        - int indexOf(String s)                         /- first occurence of a substring -/
         */
        /* indexOf(str, startIndex)
        - int indexOf(int ch, int startIndex)
        - int indexOf(String s, int startIndex)
         */
        /*lastIndexOf(str)
        - int lastIndexOf(int ch)                       /- last occurence of a character -/
        - int lastIndexOf(String s ch)                  /- last occurence of a substring -/
         */
        /* lastIndexOf(str, startIndex)
        - int lastIndexOf(int ch, int startIndex)       /- search from startIndex to zero -/
        - int lastIndexOf(String s ch, int startIndex)
         */

        String s = "Now is the time for all good men " +
                "to come to the aid of their country";
        System.out.println(s);
        System.out.println("indexOf(t) = " + s.indexOf('t'));
        System.out.println("lastIndexOf(t) = " + s.lastIndexOf('t'));
        System.out.println("indexOf(the) = " + s.indexOf("the"));
        System.out.println("lastIndexOf(the) = " + s.lastIndexOf("the"));
        System.out.println("indexOf(t, 10) = " + s.indexOf('t', 10));
        System.out.println("lastIndexOf(t, 60) = " + s.lastIndexOf('t', 60));
        System.out.println("indexOf(the, 10) = " + s.indexOf("the", 10));
        System.out.println("lastIndexOf(the, 60) = " + s.lastIndexOf("the", 60));
    }
}
