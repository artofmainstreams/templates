package foundament.string.String.modifying.trim;

/**
 * Created by frien on 28.07.2016.
 */
public class Trim {
     /* очистка пробелов с обеих сторон строки
    - String trim()
     */
     public static void main(String[] args) {
         System.out.println("        Здравствуй, Мир!         ".trim()); //Здравствуй, Мир!
     }
}
