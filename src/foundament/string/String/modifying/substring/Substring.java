package foundament.string.String.modifying.substring;

/**
 * Created by frien on 28.07.2016.
 */
public class Substring {
    /* substring
    - String substring(int startIndex)
    - String substring(int startIndex, int endIndex)
     */
    /* subSequence
    - CharSequence subSequence(int startIndex, int stopIndex)
     */
    public static void main(String[] args) {
        System.out.println("Подстрока".substring(3)); //строка
        System.out.println("Подстрока".substring(5, 8)); //рок
    }
}
