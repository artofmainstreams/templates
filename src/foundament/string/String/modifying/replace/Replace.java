package foundament.string.String.modifying.replace;

/**
 * Created by frien on 28.07.2016.
 */
public class Replace {
    /* replace
    - String replace(char original, char replacement)
    - String replace(CharSequence original, CharSequence replacement)
    - String replaceFirst(String regExp, String newStr)
    - String replaceAll(String regExp, String newStr)
     */
    public static void main(String[] args) {
        System.out.println("Замена всех букв на другую".replace('а', 'о')); //Зомено всех букв но другую
        System.out.println("Строка строке не строка".replace("строк", "булочк")); //Строка булочке не булочка
        System.out.println("Замена первого слова".replaceFirst("первого", "некоторого")); //Замена некоторого слова
        System.out.println("Замена всех-всех-всех слов".replaceAll("всех", "этих")); //Замена этих-этих-этих слов
    }
}
