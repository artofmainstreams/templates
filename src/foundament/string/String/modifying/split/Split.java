package foundament.string.String.modifying.split;

import java.util.Arrays;

/**
 * Created by frien on 28.07.2016.
 */
public class Split {
    /* split
    - String[] split(String regExp)             /- разделяет строчку на массив слов -/
    - String[] split(String regExp, int max)
     */
    public static void main(String[] args) {
        String[] arr1 = " Строку превратим в массив слов ".split(" ", 5); //разделяем на 5 слов
        String[] arr2 = " Строку превратим в массив слов ".split(" ", 0); //последний не учитываем
        String[] arr3 = " Строку превратим в массив слов ".split(" ", -1); //полностью разделяем
        System.out.println(Arrays.toString(arr1));
        System.out.println("Размер " + arr1.length);

        System.out.println(Arrays.toString(arr2));
        System.out.println("Размер " + arr2.length);

        System.out.println(Arrays.toString(arr3));
        System.out.println("Размер " + arr3.length);
    }
}
