package foundament.string.String.modifying.concat;

/**
 * Created by frien on 27.07.2016.
 */
public class Concat {
    /* concat
    - String concat(String str)
    */
    /* первый аргумент связывает последующие слова между собой
    - static String join(CharSequence delimiter, CharSequence...strs)
    - static String join(CharSequence delimiter, Iterable<? extends CharSequence> elements)
     */

    public static void main(String[] args) {
        String name = "He";
        int number = 1;
        String s = name + " is " + number + 7 + " years old";
        System.out.println(s);
        System.out.println("not four: " + 2 + 2);
        System.out.println("four: " + (2 + 2));
        String s1 = s.concat("something");

        String result = String.join("...", "Alpha", "Beta", "Gamma");
        System.out.println(result);
        result = String.join(", ", "John", "ID#: 569", "E-mail: John@HerbSchildt.com");
        System.out.println(result);
    }
}
