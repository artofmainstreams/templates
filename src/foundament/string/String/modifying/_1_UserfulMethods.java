package foundament.string.String.modifying;

/*
char charAt(int index)  //returns the code unit
int codePointAt(int index)  //returns the code point
int offsetByCodePoints(int startIndex, int cpCounts)
int compareTo(String other) //positive если строка после other in dictionary order, 0 if equal
IntStream codePoints()
new String(int[] codePoints, int offset, int count)
boolean equals(Object other)
boolean equalsIgnoreCase(String other)
boolean startsWith(String preffix)
boolean endsWith(String suffix)
int indexOf(String str)
int indexOf(String str, int fromIndex)
int indexOf(int cp)
int indexOf(int cp, int fromIndex)
int lastIndexOf(String str)
int lastIndexOf(String str, int fromIndex)
int lastindexOf(int cp)
int lastindexOf(int cp, int fromIndex)
int length()
int codePointCount(int startIndex, int endIndex)
String replace(CharSequence oldString, CharSequence newString)
String substring(int beginIndex)
String substring(int beginIndex, int endIndex)
String toLowerCase()
String toUpperCase()
String trim()
String join(CharSequence delimiter, CharSequence... elements)
 */
public class _1_UserfulMethods {
}
