package foundament.string.String.modifying.compare;

/**
 * Created by frien on 27.07.2016.
 */
public class Comparison {
    /* сравнивает строку начиная с некоторого символа с подстрокой другой строки
    - boolean regionMatches(int startIndex, String str2, int str2StartIndex, int numChars)
    - boolean regionMatches(boolean ignoreCase, int startIndex, String str2, int str2StartIndex, int numChars)
     */
    /* вхождение строки
    - boolean startsWith(String str)                    /- проверяет с начала -/
    - boolean startsWith(String str, int startIndex)    /- проверяет с указанного индекса -/
    - boolean endsWith(String str)                      /- проверяет с конца в обратном порядке -/
     */
    /* сравнение лексикографического порядка
    - int compareTo(String str)                         /- с учётом регистра -/
    - compareToIgnoreCase(String str)                   /- без учёта регистра -/
     */
    /* сравнение по значению
    - boolean containts(CharSequence str)
    - boolean contentEquals(CharSequence str)
    - boolean contentEquals(StringBuffer str)
    - boolean matches(String regExp)
    - equals()
    */
    /* сравнение по ссылке
    - ==
     */
    static String arr[] = {
        "Now", "is", "the", "time", "for", "all", "good", "men",
        "to", "come", "to", "the", "happy", "of", "their", "country"
    };
    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = "Hello";
        String s3 = "Good-bye";
        String s4 = "HELLO";
        System.out.println(s1 + " equals " + s2 + " -> " + s1.equals(s2));
        System.out.println(s1 + " equals " + s3 + " -> " + s1.equals(s3));
        System.out.println(s1 + " equals " + s4 + " -> " + s1.equals(s4));
        System.out.println(s1 + " equalsIgnoreCase " + s4 + " -> " + s1.equalsIgnoreCase(s4));

        System.out.println(s1 + " equals " + s3 + " -> " + s1.regionMatches(1, s3, 7, 1));

        System.out.println("Foobar".endsWith("bar"));
        System.out.println("Foobar".startsWith("Foo"));
        System.out.println("Foobar".startsWith("bar", 3));
        //equals() and ==
        String s5 = new String(s1);
        System.out.println(s1 + " equals " + s2 + " -> " + s1.equals(s5));
        System.out.println(s1 + " == " + s2 + " -> " + (s1 == s5));
        System.out.println(s1 + " matches " + s2 + " -> " + s1.matches(s5));

        for (int j = 0; j < arr.length; j++) {
            for (int i = j + 1; i < arr.length; i++) {
                if (arr[i].compareTo(arr[j]) < 0) {
                    String t = arr[j];
                    arr[j] = arr[i];
                    arr[i] = t;
                }
            }
            System.out.println(arr[j]);
        }
    }
}
