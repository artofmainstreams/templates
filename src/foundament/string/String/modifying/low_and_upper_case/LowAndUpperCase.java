package foundament.string.String.modifying.low_and_upper_case;

/**
 * Created by frien on 28.07.2016.
 */
public class LowAndUpperCase {
    /* changing the Case of characters
    - String toLowerCase()
    - String toUpperCase()
     */
    public static void main(String[] args) {
        System.out.println("Сделаем же все буквы большими".toUpperCase()); //СДЕЛАЕМ ЖЕ ВСЕ БУКВЫ БОЛЬШИМИ
        System.out.println("СдЕлАеМ жЕ вСе БуКвЫ мАлЕнЬкИмИ".toLowerCase()); //сделаем же все буквы маленькими
    }
}
