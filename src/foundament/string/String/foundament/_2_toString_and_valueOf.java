package foundament.string.String.foundament;

/**
 * Created by frien on 27.07.2016.
 */
public class _2_toString_and_valueOf {
    /* converts data from its internal format into a human-readable form
    - static String valueOf(double num)
    - static String valueOf(long num)
    - static String valueOf(Object ob)
    - static String valueOf(char chars[])
    - static String valueOf(char chars[], int startIndex, int numChars)
     */
    @Override
    public String toString() {
        return "Переопределяем метод";
    }

    public static void main(String[] args) {
        _2_toString_and_valueOf ob = new _2_toString_and_valueOf();
        System.out.println(ob);
        System.out.println(String.valueOf(ob));
    }
}
