package foundament.string.String.foundament;

/**
 * Created by artofmainstreams on 09.04.2017.
 */
public class _3_UTF16 {
    //TODO разобраться подробнее
    public static void main(String[] args) {
        String sentence = "\ud835\udd46 is the set of octonions";

        char ch = sentence.charAt(1);   //вернёт second code unit, не делай так

        //если код перемещается по строке и хочешь посмотреть на каждый code point
        int i = 0;
        int codePoint = sentence.codePointAt(i);
        if (Character.isSupplementaryCodePoint(codePoint)) i += 2;
        else i++;
        //можно и назад вернуться
        i--;
        if (Character.isSurrogate(sentence.charAt(i))) i--;
        codePoint = sentence.codePointAt(i);
        //но это всё довольно сложно, можно проще
        int[] codePoints = sentence.codePoints().toArray();
        //если обратно, codePoints в строку, то
        String str = new String(codePoints, 0, codePoints.length);

        System.out.println(sentence);
    }
}
