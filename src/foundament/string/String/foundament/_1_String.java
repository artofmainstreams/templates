package foundament.string.String.foundament;

/**
 * Created by frien on 27.07.2016.
 */
public class _1_String {
    static int number = 8;
    static char chars[] = {'a', 'b', 'c', 'd', 'e', 'f'};
    static byte ascii[] = {65, 66, 67, 68, 69, 70};
    static int[] arr = {67, 78, 83};
    //The String Constructor
    static String s1 = new String();
    static String s2 = new String(chars);
    //String(char chars[], int startIndex, int numChars)
    static String s3 = new String(chars, 2, 3); //cde
    static String s4 = new String(s3);
    static String s5 = new String(ascii);
    static String s6 = new String(ascii, 2, 3);
    static String s7 = new String(new StringBuffer("string"));
    static String s8 = new String(new StringBuilder("string"));
    static String s9 = new String(arr, 0, 2);
    //-
    static String myString = "my string" + " is greathly than " + number;

    public static void main(String[] args) {
        System.out.println(myString);
        if (myString != null && myString.equals("")) {
            System.out.println("Строка пустая");
        }
        if (myString.equals("my string is greathly than 8")) {
            System.out.println("Эквивалентно");
        }
        if (myString.equalsIgnoreCase("my string is greathly than 8")) {
            System.out.println("Эквивалентно без учёта регистра");
        }
        System.out.println("Длина строки: " + myString.length());
        System.out.println("Символ #8: " + myString.charAt(7));
        System.out.println("Длина некоторой строки: " + "некоторая строка".length());
    }
}
