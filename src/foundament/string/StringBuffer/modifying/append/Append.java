package foundament.string.StringBuffer.modifying.append;

/**
 * Created by frien on 28.07.2016.
 */
public class Append {
    /* append
    - StringBuffer append(String str)
    - StringBuffer append(int num)
    - StringBuffer append(Object obj)
    - StringBuffer appendCodePoint(int ch)          /- добавляет в конец Unicode-символ -/
     */
    public static void main(String[] args) {
        String s;
        int a = 42;
        StringBuffer sb = new StringBuffer(40);
        s = sb.append("a = ").append(a).append("!").toString();
        System.out.println(s);
    }
}
