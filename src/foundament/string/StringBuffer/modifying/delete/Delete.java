package foundament.string.StringBuffer.modifying.delete;

/**
 * Created by frien on 28.07.2016.
 */
public class Delete {
    /* delete
    - StringBuffer delete(int startIndex, int endIndex)
    - StringBuffer deleteCharAt(inc loc)
     */
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("This is a test");

        sb.delete(4, 7);
        System.out.println("After delete: " + sb);

        sb.deleteCharAt(0);
        System.out.println("After deleteCharAtL: " + sb);
    }
}
