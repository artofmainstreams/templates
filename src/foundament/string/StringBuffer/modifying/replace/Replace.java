package foundament.string.StringBuffer.modifying.replace;

/**
 * Created by frien on 28.07.2016.
 */
public class Replace {
    /* replace
    - StringBuffer replace(int startIndex, int endIndex, String str)
     */
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("This is a test");

        sb.replace(5, 7, "was");
        System.out.println("After replace: " + sb);
    }
}
