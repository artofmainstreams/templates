package foundament.string.StringBuffer.modifying.reverse;

/**
 * Created by frien on 28.07.2016.
 */
public class Reverse {
    /* reverse
    - StringBuffer reverse()
     */
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("12345");
        System.out.println(sb);
        sb.reverse();
        System.out.println(sb);
    }
}
