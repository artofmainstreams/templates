package foundament.string.StringBuffer.modifying.insert;

/**
 * Created by frien on 28.07.2016.
 */
public class Insert {
    /* insert
    - StringBuffer insert(int index, String str)
    - StringBuffer insert(int index, char ch)
    - StringBuffer insert(int index, Object obj)
     */
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("I Java!");
        sb.insert(2, "like ");
        System.out.println(sb);
    }
}
