package foundament.string.StringBuffer.modifying.char_extract;

/**
 * Created by frien on 28.07.2016.
 */
public class CharacterExtraction {
    /* charAt()
    - char charAt(int where)
    - void setCharAt(int where, char ch)
     */
    /* скопировать в массив подстроку
    - void getChars(int sourceStart, int sourceEnd, char target[], int targetStart)
     */
    /* codePoint
    - int codePointAt(int index)        /- возвращает Unicode-символ -/
    - int codePointBefore(int index)
    - int codePointCount(int start, int end)
     */
    /* offsetByCodePoints
    - int offsetByCodePoints(int start, int num)
     */
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("Hello");
        System.out.println("buffer before = " + sb);
        System.out.println("charAt(1) before = " + sb.charAt(1));

        sb.setCharAt(1, 'i');
        sb.setLength(2);

        System.out.println("buffer after = " + sb);
        System.out.println("charAt(1) after = " + sb.charAt(1));
    }
}
