package foundament.string.StringBuffer.foundament;

/**
 * Created by frien on 27.07.2016.
 */
public class _1_StringBuffer {
    /* Constructor
    - StringBuffer()                /- содержит резерв для 16 characters -/
    - StringBuffer(int size)        /- размер буфера -/
    - StringBuffer(String str)      /- содержит строку и резерв для 16 characters -/
    - StringBuffer(CharSequence)
     */
    /*
    - int length()
    - int capacity()
    - void ensureCapacity(int minCapacity)      /- увеличить минимальную вместимость буфера -/
    - void setLength(int len)                   /- установить длину строки -/
     */
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("  Hello  ");

        sb.setLength(30);
        System.out.println("buffer = " + sb);
        System.out.println("length = " + sb.length());
        System.out.println("capacity = " + sb.capacity());
    }
}
