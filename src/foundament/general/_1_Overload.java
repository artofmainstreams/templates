package foundament.general;

/**
 * Created by frien on 27.07.2016.
 */
public class _1_Overload {
    static void method() {}
    static void method(double arg) {} //overloading method
    static void method(String...args) {}
    static void method(float... args) {}

    _1_Overload() {
        method(8, 7); //automatic type conversions
    }

    _1_Overload(int arg) {
        method(arg); //automatic type conversions
    }

    public static void main(String[] args) {
        _1_Overload ob = new _1_Overload();
    }
}
