package foundament.general;

/**
 * Created by frien on 27.07.2016.
 */
public class _6_InnerClass {
    public static void main(String[] args) {
        Outer outer = new Outer();
        outer.test();
    }
}

class Outer {
    int outerX = 100;

    class Inner { //non-static class
        int y;
        void display() {
            System.out.println("display: outerX = " + outerX);
        }
    }

    void test() {
        Inner inner = new Inner();
        inner.display();
    }
}
