package foundament.general;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by frien on 27.07.2016.
 */
public class _5_Array {
    public static void main(String[] args) {
        int a0[]; //здесь мы просто объявили переменную, ничего не создали
        int a1[] = new int[10]; //все нули, для boolean - false
        int a2[] = {1, 2, 3, 4, 5, 6, 7};
        String[] a3 = new String[10];   //все null
        //str - локальная переменная
        for (String str : a3) {
            System.out.println(str);
        }

        System.out.println("Длина массива: " + a1.length);
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        a0 = new int[n];
        System.out.println("Длина массива: " + a0.length);

        //TODO Копирование массива
        a1 = Arrays.copyOf(a2, a2.length);  //второй параметр - длина нового массива
        System.out.println("Длина массива: " + a1.length);
        //TODO QuickSort
        Arrays.sort(a1);
        //TODO binarySearch
        int n1 = Arrays.binarySearch(a1, 8);    //массив должен быть сортированным
        //TODO fill одним значением
        Arrays.fill(a3, "строка");
        //TODO equals
        if (a0.equals(a1)) {
            System.out.println("true");
        }
        //TODO Двумерные массивы
        double[][] array = new double[7][8];
        for (double[] row:array) {
            for (double value:row) {
                //do something
            }
        }
        System.out.println(Arrays.deepToString(array));
    }
}
