package foundament.general;

/**
 * Created by frien on 27.07.2016.
 */
public class _2_Access {
    /* access
    - public = всё доступно
    - (ничего не ставится) = всё доступно в пределах пакета
    - protected = доступно только наследникам
    - private = доступно только этому классу
     */
}
