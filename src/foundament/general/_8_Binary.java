package foundament.general;

import java.util.Scanner;

/*
    Побитовые операции:
    & | ^ !
    >> - сдвиг вправо, первый бит не меняем
    >>> - сдвигаем все биты
    <<
 */
public class _8_Binary {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int fourthBitFromRight = (n & 0b1000) / 0b1000;
        System.out.println(fourthBitFromRight); //либо 1, либо 0
    }
}
