package foundament.general;

import java.lang.Class;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class _10_Class_And_Object implements Cloneable{
    public int value = 11;
    public String str = "Hello";
    private String vls;

    public _10_Class_And_Object(String v){
        vls =v;
    }

    public _10_Class_And_Object(){
        vls = "";
    }

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchFieldException, NoSuchMethodException {
        //получаем объект, который хранит всю информацию о нашем классе
        Class cl = _10_Class_And_Object.class;
        System.out.println(cl.getName());
        System.out.println(cl.getCanonicalName());

        //создаём объект по названию
        Object nw = Class.forName("foundament.general._10_Class_And_Object").newInstance();
        System.out.println(nw instanceof _10_Class_And_Object);

        //получаем имена полей
        System.out.println(cl.getField("value"));
        Field[] fa = cl.getFields();
        System.out.println(Arrays.toString(fa));

        Class[] cls = cl.getInterfaces();
        System.out.println(Arrays.toString(cls));

        Method[] ma = cl.getMethods();
        System.out.println(Arrays.toString(ma));

        Method[] dma = cl.getDeclaredMethods();
        System.out.println(Arrays.toString(dma));

        Constructor[] ca = cl.getConstructors();
        System.out.println(Arrays.toString(ca));

        Constructor[] dca = cl.getDeclaredConstructors();
        System.out.println(Arrays.toString(dca));
    }

    public void myMethod(int helper){

    }
}
