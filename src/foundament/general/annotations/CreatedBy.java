package foundament.general.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by artofmainstreams on 06.06.2017.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CreatedBy {
    String author();
    String date();
}
