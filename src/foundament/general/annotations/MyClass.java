package foundament.general.annotations;

import java.lang.reflect.Method;
import java.util.logging.Logger;


/**
 * Created by artofmainstreams on 06.06.2017.
 */
@CreatedBy(author = "я", date = "сегодня")
public class MyClass {

    @CreatedBy(author = "кто-то", date = "некоторый день")
    public static void main(String[] args) throws NoSuchMethodException {
        Class myClass = MyClass.class;

        Class[] paramTypes = new Class[] {String[].class};
        Method method = myClass.getMethod("main", paramTypes);
        CreatedBy ann1 = (CreatedBy) myClass.getAnnotation(CreatedBy.class);
        CreatedBy ann2 = method.getAnnotation(CreatedBy.class);

        Logger.getLogger("MyLogger").info("Author of the class: " + ann1.author());
        Logger.getLogger("MyLogger").info("Дата создания: " + ann1.date());
        Logger.getLogger("MyLogger").info("Author of the class: " + ann2.author());
        Logger.getLogger("MyLogger").info("Дата создания: " + ann2.date());
    }
}
