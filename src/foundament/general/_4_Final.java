package foundament.general;

/**
 * Created by frien on 27.07.2016.
 */
public class _4_Final {
    final int CONSTANT = 1;
    final int OTHER_CONSTANT;   //подходит для primitive и immutable class
    final StringBuilder stringBuilder;  //теперь всегда ссылается на этот элемент

    public _4_Final() {
        OTHER_CONSTANT = 7; //можно и здесь, но так не принято
        stringBuilder = new StringBuilder();    //объект можно менять
    }
}
