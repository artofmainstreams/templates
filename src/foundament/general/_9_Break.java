package foundament.general;

/*
Выход из нескольких циклов
 */
public class _9_Break {
    public static void main(String[] args) {
        extra_exit:
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j == 3) {
                    continue;
                }
                if (i == 8 && j == 5) {
                    System.out.println(i + ":" + j);
                    break extra_exit;
                }
            }
        }
        //выходим сюда
    }
}
