package foundament.general;

/**
 * Created by frien on 27.07.2016.
 */
public class _7_Varargs {
    static void test(int a, int... arg) {
        System.out.print("Количество аргументов: " + arg.length + " содержит следующие: ");
        for (int x : arg) {
            System.out.print(x + " ");
        }
        System.out.println();
    }

    public static void main(String...args) {
        test(10);
        test(1, 2, 3);
        test(1);
    }
}
