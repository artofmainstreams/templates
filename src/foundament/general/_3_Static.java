package foundament.general;

/**
 * Created by frien on 27.07.2016.
 */
public class _3_Static {
    /* static
    - can only directly call other static methods
    - can only directly access static data
    - cannot refer to this or super in any way
     */
    static int a = 8;
    static {
        System.out.println("Выполняется при загрузке класса, даже до main, a = " + a);
    }

    public static void main(String[] args) {
        System.out.println("Чуть позже запускается");
    }
}
