package foundament.other;

/**
 * Created by frien on 26.07.2016.
 */
public class _4_native {
    int i;

    public static void main(String[] args) {
        _4_native ob = new _4_native();
        ob.i = 10;
        System.out.println("This is ob.i before the native method:" + ob.i);
        ob.test(); //call a native method
        System.out.println("This is ob.i after the native method:"  + ob.i);
    }
    //declare native method
    public native void test();
    //load DLL that containts satic method
    static { //но такой библиотеки не создавал
        System.loadLibrary("NativeDemo"); //dynamic link
    }
}
