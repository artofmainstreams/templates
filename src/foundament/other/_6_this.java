package foundament.other;

/**
 * Created by frien on 26.07.2016.
 */
public class _6_this {
    /* restrictions:
    - cannot use any instance variable of the constructor's class in a call this()
    - cannot use super() and this() in the same constructor because each must be
    the first statement in the constructor
     */
    int a;
    int b;

    _6_this(int a, int b) {
        this.a = a;
        this.b = b;
    }

    _6_this(int i) {
        this(i, i);
    }

    _6_this() {
        this(0);
    }

    public static void main(String[] args) {
        new _6_this(7);
    }
}
