package foundament.other;

import java.math.BigInteger;

/**
 * Created by artofmainstreams on 09.04.2017.
 */
public class _7_BigNumbers {
    public static void main(String[] args) {
        System.out.println(factorial(50));
    }

    public static BigInteger factorial(long n)
    {
        BigInteger a = BigInteger.valueOf(1);
        for (long i = 1; i <= n; ++i) a = a.multiply(BigInteger.valueOf(i));
        return a;
    }
}
