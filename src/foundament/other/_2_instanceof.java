package foundament.other;

/**
 * Created by frien on 26.07.2016.
 */
public class _2_instanceof {
    public static void main(String[] args) {
        Figure figure = new Figure();
        Point point = new Point();
        Circle circle = new Circle();
        Square square = new Square();
        if (figure instanceof Figure) {
            System.out.println("figure is instance of Figure");
        }
        if (point instanceof Point) {
            System.out.println("point is instance of Point");
        }
        if (circle instanceof Circle) {
            System.out.println("circle is instance of Circle");
        }
        if (circle instanceof Figure) {
            System.out.println("circle can be cast to Figure");
        }
        if (figure instanceof Circle) {
            System.out.println("figure can be cast to Circle");
        }
        System.out.println();
        Figure object;
        object = square;
        System.out.println("objects now refers to square");
        if (object instanceof Square) {
            System.out.println("object is instance of Square");
        }
        System.out.println();
        object = circle;
        System.out.println("objects now refers to circle");
        if (object instanceof Square) {
            System.out.println("object can be cast to Square");
        } else {
            System.out.println("object cannot be cast to Square");
        }
        if (object instanceof Figure) {
            System.out.println("object can be instance of Figure");
        }
        System.out.println();
    }
}

class Figure {
    int x, y;
}

class Point {
    int x, y;
}

class Circle extends Figure {
    int size;
}

class Square extends Figure {
    int size;
}