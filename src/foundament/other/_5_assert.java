package foundament.other;

/**
 * Created by frien on 26.07.2016.
 */
public class _5_assert {
    static int val = 3;

    static int getNum() {
        return val--;
    }

    public static void main(String[] args) {
        int n;

        for (int i = 0; i < 10; i++) {
            n = getNum();
            try { //необязательно try-catch
                assert n > 0 : "n is negative"; //-ea or -da
                assert n > 0; //можно и так: -ea:название пакета или класса
            } catch (AssertionError e) {
                System.out.println(e);
            }

            System.out.println("n is " + n);
        }
    }
}
