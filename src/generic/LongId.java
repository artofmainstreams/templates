package generic;

/**
 * Created by artofmainstreams on 31.05.2017.
 */
public class LongId<T> {
    private static long counter = 0;
    private long id;
    public LongId() {
        id = ++counter;
    }
    public long getId() {
        return id;
    }
}
