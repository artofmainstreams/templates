package generic;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by artofmainstreams on 01.06.2017.
 */
public class ComparableClass implements Comparable {
    public int i;

    @Override
    public int compareTo(Object o) {

        if (o instanceof ComparableClass) {
            return i > ((ComparableClass) o).i ? 7 : -7;
        }
        return 0;
    }

    public static void main(String[] args) {
        ComparableClass a = new ComparableClass();
        ComparableClass b = new ComparableClass();
        ComparableClass c = new ComparableClass();
        a.i = 2;
        b.i = 1;
        c.i = 3;
        List<ComparableClass> list = new LinkedList();
        list.add(a);
        list.add(b);
        list.add(c);
        Collections.sort(list);
        for (ComparableClass comparableClass : list) {
            System.out.print(comparableClass.i + " : ");
        }
    }
}
