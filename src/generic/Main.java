package generic;

/**
 * Created by artofmainstreams on 31.05.2017.
 */
public class Main {

    public static void manyIdsInParams(LongId<User> user) {
        System.out.println(user.getId());
    }

    public static void main(String[] args) {
        User user1 = new User();
        User user2 = new User();
        manyIdsInParams(user1.idUser);
        manyIdsInParams(user2.idUser);
    }
}
