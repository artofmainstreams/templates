package generic;

import java.util.List;

/**
 * Created by artofmainstreams on 30.05.2017.
 */
public class _1_ {
    /**
     * @param E можно делать всё что угодно, но возвращает всегда Object
     */
    public static void function1(List<? extends Object> E) {}

    /**
     * @param E можно делать всё что угодно, кроме вставки
     */
    public static void function2(List<?> E) {}

    /**
     * @param E можно передавать суперклассы
     */
    public static void function3(List<? super Number> E) {}
}
