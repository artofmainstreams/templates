package applet;

import java.applet.Applet;
import java.awt.*;

/*
 <applet code="_1_" width="200 height=60"></applet>
 */
public class _1_ extends Applet {
    @Override
    public void paint(Graphics g) {
        //void drawString(String message, int x, int y)
        g.drawString("A Simple Applet", 20, 20);
    }
}
