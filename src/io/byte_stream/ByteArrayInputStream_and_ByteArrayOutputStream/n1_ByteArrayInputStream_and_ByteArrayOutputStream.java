package io.byte_stream.ByteArrayInputStream_and_ByteArrayOutputStream;

import java.io.*;

/**
 * Created by frien on 31.07.2016.
 */
public class n1_ByteArrayInputStream_and_ByteArrayOutputStream {
    /* Constructors ByteArrayInputStream            /- метод close() необязателен, есть mark() и reset() -/
    - ByteArrayInputStream(byte array[])
    - ByteArrayInputStream(byte array[], int start, int numBytes)
     */
    /* Constructors ByteArrayOutputStream           /- метод close() необязателен -/
    - ByteArrayOutputStream()                       /- буфер для 32 байт -/
    - ByteArrayOutputStream(int numBytes)
     */
    public static void main(String[] args) {
        String str = "abcdefghijklmnopqrstuvwxyz";
        byte b[] = str.getBytes();

        ByteArrayInputStream input = new ByteArrayInputStream(b);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        //input
        for (int i = 0; i < 2; i++) { //2 раза печатаем
            int c;
            while((c = input.read()) != -1) {
                if (i == 0) {
                    System.out.print((char) c);
                } else {
                    System.out.print((Character.toUpperCase((char) c)));
                }
            }
            System.out.println();
            input.reset();
        }
        //output
        try {
            output.write(b);    //записываем
        } catch (IOException e) {
            System.out.println("Error writting to buffer");
        }
        System.out.println("Буфер как строка:");
        System.out.println(output.toString());  //выводим на экран
        System.out.println("Буфер в массив");
        byte b1[] = output.toByteArray();       //возвращаем массив байт
        for (int i = 0; i < b.length; ++i) {
            System.out.print((char) b1[i]);
        }
        System.out.println("\nВ выходной поток (OutputStream)");
        try (FileOutputStream file = new FileOutputStream("test.txt")) {
            output.writeTo(file);               //записываем в файл
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Doing a reset");
        output.reset();
        for (int i = 0; i < 3; ++i) {
            output.write('X');
        }
        System.out.println(output.toString());
    }
}
