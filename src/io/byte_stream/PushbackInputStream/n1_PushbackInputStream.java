package io.byte_stream.PushbackInputStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PushbackInputStream;

/**
 * Created by frien on 31.07.2016.
 */
public class n1_PushbackInputStream {
    /* Constructor          /- позволяет подглядеть в поток -/
    - PushbackInputStream(InputStream inputStream)
    - PushbackInputStream(InputStream inputStream, int numBytes)
     */
    /* methods
    - void unread(int b)    /- положить младший байт -/
    - void unread(byte buffer[])
    - void unread(byte buffer[], int offset, int numBytes)
     */

    public static void main(String[] args) {
        String s = "if (a == 4) a = 0;\n";
        byte buf[] = s.getBytes();
        ByteArrayInputStream in = new ByteArrayInputStream(buf);
        int c;

        try (PushbackInputStream file = new PushbackInputStream(in)) {
            while ((c = file.read()) != -1) {
                switch(c) {
                    case '=':
                        if ((c = file.read()) == '=') {
                            System.out.print(".eq.");
                        } else {
                            System.out.print("<-");
                            file.unread(c);
                        }
                        break;
                    default:
                        System.out.print((char)c);
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
