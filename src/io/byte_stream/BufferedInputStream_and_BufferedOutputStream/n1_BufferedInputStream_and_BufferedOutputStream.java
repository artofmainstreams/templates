package io.byte_stream.BufferedInputStream_and_BufferedOutputStream;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created by frien on 31.07.2016.
 */
public class n1_BufferedInputStream_and_BufferedOutputStream {
    /* Constructors BufferedInputStream
    - BufferedInputStream(InputStream inputStream)
    - BufferedInputStream(InputStream inputStream, int bufSize)
     */
    /* Constructors BufferedOutputStream        /- включает метод flush() -/
    - BufferedOutputStream(OutputStream outputStream)
    - BufferedOutputStream(OutputStream outputStream, int bufSize)
     */
    public static void main(String[] args) {
        String s = "This is a &copy; copyringht symbol but this is &copy not\n";
        byte[] buf = s.getBytes();
        ByteArrayInputStream in = new ByteArrayInputStream(buf);
        int c;
        boolean marked = false;

        try (BufferedInputStream file = new BufferedInputStream(in)) {
            while ((c = file.read()) != -1) {
                switch (c) {
                    case '&':   //ставим метку, ничего не печатаем
                        if (!marked) {
                            file.mark(32);
                            marked = true;
                        } else {
                            marked = false;
                        }
                        break;
                    case ';':   //снимаем метку, печатаем спецсимвол
                        if (marked) {
                            marked = false;
                            System.out.print("(c)");
                        } else {
                            System.out.print((char) c);
                        }
                        break;
                    case ' ':   //возвращаемся к метке, снимаем и печатаем дальше
                        if (marked) {
                            marked = false;
                            file.reset();
                            System.out.print("&");
                        } else {
                            System.out.print((char) c);
                        }
                        break;  //печатаем символ
                    default:
                        if (!marked) {
                            System.out.print((char) c);
                        }
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
