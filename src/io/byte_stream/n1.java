package io.byte_stream;

import java.io.*;

/**
 * Created by frien on 25.07.2016.
 */
public class n1 {
    /*************/
    InputStream ob1;           //Abstract class that describes stream input
    OutputStream ob2;          //Abstract class that describes stream output
    //read()
    //write()
    /*************/
    BufferedInputStream ob3;    //Buffered input stream
    BufferedOutputStream ob4;   //Buffered output stream

    ByteArrayInputStream ob5;   //Input stream that reads from a byte array
    ByteArrayOutputStream ob6;  //Output stream that writes to a byte array

    DataInputStream ob7;        //An input stream that contains methods for reading the Java
                                //standart data types
    DataOutputStream ob8;       //An output stream that contains methods for writting the Java
                                //standart data types

    FileInputStream ob9;        //Input stream that reads from a file
    FileOutputStream ob10;      //Output stream that writes to a file

    FilterInputStream ob11;     //Implements InputStream
    FilterOutputStream ob12;    //implements OutputStream


    ObjectInputStream ob13;     //Input stream for objects
    ObjectOutputStream ob14;    //Output stream for objects

    PipedInputStream ob15;      //Input pipe
    PipedOutputStream ob16;     //Output pipe

    PrintStream ob17;           //Output stream that contains print() and println()
    PushbackInputStream ob18;   //Input stream that supports one-byte "unget", which returns a
                                //byte to the input stream
    SequenceInputStream ob19;   //Input stream that is a combination of two or more input
                                //streams that will be read sequentially, one after the other
}
