package io.byte_stream.InputStream_and_OutputStream;

/**
 * Created by frien on 30.07.2016.
 */
public class n1_InputStream_and_OutputStream {
    /* InputStream
    - int available()           /- доступные для чтения байты -/
    - void close()              /- закрывает входные данные -/
    - void mark(int numBytes)   /- mark at the current point that will remain valid until numBytes read -/
    - boolean markSupported()   /- возвращает true/false, если mark()/reset() оддерживаются -/
    - int read()                /- возвращает целочисленное представление следующего доступного байта
                                   или -1, если файл закончился -/
    - int read(byte buffer[])   /- возвращает количество считанных в buffer байтов или -1, если файл закончился -/
    - int read(byte buffer[],   /- читает numBytes, начиная с buffer[offset] -/
              int offset[],
              int numBytes)
    - void reset()              /- сбрасывает текущий указатель к предыдущей метке -/
    - long skip(long numBytes)  /- игнорирует numBytes байтов и возвращает количество успешно пропущенных -/
     */
    /* OutputStream
    - void close()
    - void flush()
    - void write(int b)
    - void write(byte buffer[])
    - void write(byte buffer[], int offset, int numBytes)
     */
}
