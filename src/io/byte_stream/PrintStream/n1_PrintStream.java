package io.byte_stream.PrintStream;

/**
 * Created by frien on 25.07.2016.
 */
public class n1_PrintStream {
    public static void main(String[] args) {
        /* Constructor                              /- System.out - объект PrintStream -/
        - PrintStream(OutputStream outputStream)
        - PrintStream(OutputStream outputStream, boolean autoFlushingOn)
        - PrintStream(OutputStream outputStream, boolean autoFlushingOn, String charSet)
        - PrintStream(File outputFile)
        - PrintStream(File outputFile, String charSet)
        - PrintStream(String outputFileName)
        - PrintStream(String outputFileName, String charSet)
         */
        /* methods
        - print
        - println
        - PrintStream printf(String fmtString, Object...args)
        - PrintStream printf(Locale loc, String fmtString, Object...args)
        - PrintStream format(String fmtString, Object...args)
        - PrintStream format(Locale loc, String fmtString, Objects...args)
         */
        int b;
        b = 'A';
        System.out.write(b);
        System.out.write('\n');
        System.out.print((char) b);// так легче!
    }
}
