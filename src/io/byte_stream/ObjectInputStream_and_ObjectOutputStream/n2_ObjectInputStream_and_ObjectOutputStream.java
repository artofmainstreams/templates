package io.byte_stream.ObjectInputStream_and_ObjectOutputStream;

/**
 * Created by frien on 07.08.2016.
 */
public class n2_ObjectInputStream_and_ObjectOutputStream {
    /* Constructors ObjectInputStream
    - ObjectInputStream(InputStream inStream)
     */
    /* Constructors ObjectOutputStream
    - ObjectOutputStream(OutputStream outStream)
     */
    /* methods ObjectInputStream
    - int available()
    - void close()
    - int read()
    - int read(byte buffer[], int offset, int numBytes)
    - Boolean readBoolean()
    - byte readByte()
    - char readChar()
    - double readDouble()
    - float readFloat()
    - void readFully(byte buffer[])
    - void readFully(byte buffer[], int offset, int numBytes)
    - int readInt()
    - long readLong()
    - final Object readObject()
    - short readShort()
    - int readUnsignedByte()
    - int readUnsignedShort()
     */
    /* methods ObjectOutputStream
    - void close()
    - void flush()
    - void write(byte buffer[])
    - void write(byte buffer[], int offset, int numBytes)
    - void write(int b)
    - void writeBoolean(boolean b)
    - void writeByte(int b)
    - void writeBytes(String str)
    - void writeChar(int c)
    - void writeChars(String str)
    - void writeDouble(double d)
    - void writeFloat(float f)
    - void writeInt(int i)
    - void writeLong(long l)
    - final void writeObject(Object obj)
    - void writeShort(int i)
     */
}
