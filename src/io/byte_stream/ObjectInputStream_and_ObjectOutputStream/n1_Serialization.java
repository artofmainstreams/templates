package io.byte_stream.ObjectInputStream_and_ObjectOutputStream;

/**
 * Created by frien on 07.08.2016.
 */
public class n1_Serialization {
    /* Interfaces
    - Serializable          /- используется чтобы указать, что объект можно сериализовать -/
    - Externalizable        /- используется для особой сериализации (сжатие, шифрование) -/
    - ObjectOutput
     */
    /* methods Externalizable
    - void readExternal(ObjectInput inStream)
    - void writeExternal(ObjectOutput outStream)
     */
    /* methods ObjectInput
    - int available()
    - void close()
    - int read()
    - int read(byte buffer[])
    - int read(byte buffer[], int offset, int numBytes)
    - Object readObject()
    - long skip(long numBytes)
     */
    /* methods ObjectOutput
    - void close()
    - void flush()
    - void write(byte buffer[])
    - void write(byte buffer[], int offset, int numBytes)
    - void write(int b)
    - void writeObject(Object obj)                          /- сериализует объект -/
     */
}
