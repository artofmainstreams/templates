package io.byte_stream.ObjectInputStream_and_ObjectOutputStream;

import java.io.*;

/**
 * Created by frien on 07.08.2016.
 */
public class n3_Xmpl {
    public static void main(String[] args) {
        //serialization
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("serial"))) {
            MyClass objectOne = new MyClass("Hello", -7, 2.7e10);
            System.out.println("objectOne: " + objectOne);
            objectOutputStream.writeObject(objectOne);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //deserialization
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("serial"))) {
            MyClass objectTwo = (MyClass) objectInputStream.readObject();
            System.out.println("objectTwo: " + objectTwo);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class MyClass implements Serializable {
    String s;
    int i;
    double d;

    public MyClass(String s, int i, double d) {
        this.s = s;
        this.i = i;
        this.d = d;
    }

    @Override
    public String toString() {
        return "s = " + s + "; i = " + i + "; d = " + d;
    }
}