package io.byte_stream.DataInputStream_and_DataOutputStream;

import java.io.*;

/**
 * Created by frien on 31.07.2016.
 */
public class n1_DataInputStream_and_DataOutputStream {
    /* Constructors DataInputStream
    - DataInputStream(InputStream inpuStream)
     */
    /* Constructors DataOutputStream
    - DataOutputStream(OutputSream outputStream)
     */
    /* methods DataInputStream
    - final double readDouble()
    - final boolean readBoolean()
    - final int readInt()
     */
    /* methods DataOutputStream
    - final void writeDouble(double value)
    - final void writeBoolean(boolean value)
    - final void writeInt(int value)
     */
    public static void main(String[] args) {
        try (DataOutputStream dout = new DataOutputStream(new FileOutputStream("Test.dat"))) {
            dout.writeDouble(98.6);
            dout.writeInt(1000);
            dout.writeBoolean(true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (DataInputStream din = new DataInputStream(new FileInputStream("Test.dat"))) {
            double d = din.readDouble();
            int i = din.readInt();
            boolean b = din.readBoolean();
            System.out.println("Here are the values: " + d + " " + i + " " + b);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
