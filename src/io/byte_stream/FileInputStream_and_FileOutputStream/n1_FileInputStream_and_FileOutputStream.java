package io.byte_stream.FileInputStream_and_FileOutputStream;

import java.io.*;
import java.util.Scanner;

/**
 * Created by frien on 25.07.2016.
 */
public class n1_FileInputStream_and_FileOutputStream {
    /* Constructors FileInputStream         /- mark() and reset() are not overrides -/
    - FileInputStream(String fileName) throws FileNotFoundException
    - FileInputStream(File fileObj)
    - FileOutputStream(String fileName) throws FileNotFoundException
    - FileOutputStream
     */
    /* Constructors FileOutputStream
    - FileOutputStream(String filePath)
    - FileOutputStream(File fileObj)
    - FileOutputStream(String filePath, boolean append)
    - FileOutputStream(File fileObj, boolean append)
     */
    public static void main(String[] args) { //копирование файла по одному пути в другой

        //close
        int i;
        Scanner scanner = new Scanner(System.in);
        try (FileInputStream fin = new FileInputStream(scanner.nextLine());
             FileOutputStream fout = new FileOutputStream(scanner.nextLine())) {
            System.out.print("First file: ");
            System.out.print("Second file: ");
            do {
                i = fin.read();
                if (i != -1) {
                    fout.write(i);
                }
            } while (i != -1);
        } catch (FileNotFoundException e) {
            System.out.println("Cannot open file");
        } catch (IOException e) {
            System.out.println("Error reading file");
        }
    }
}
