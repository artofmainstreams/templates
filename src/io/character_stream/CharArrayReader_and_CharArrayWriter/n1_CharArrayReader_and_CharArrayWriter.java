package io.character_stream.CharArrayReader_and_CharArrayWriter;

import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by frien on 06.08.2016.
 */
public class n1_CharArrayReader_and_CharArrayWriter {
    /* Constructors CharArrayReader
    - CharArrayReader(char array[])
    - CharArrayReader(char array[], int start, int numChars)
     */
    /* Constructors CharArrayWriter
    - CharArrayWriter()
    - CharArrayWriter(int numChars)
     */
    public static void main(String[] args) {
        String tmp = "abcdefghijklmnopqrstuvwxyz";
        int length = tmp.length();
        char[] c = new char[length];
        tmp.getChars(0, length, c, 0);
        //reader
        int i;

        try (CharArrayReader inputOne = new CharArrayReader(c)) {
            System.out.print("inputOne is: ");
            while ((i = inputOne.read()) != -1) {
                System.out.print((char)i);
            }
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (CharArrayReader inputOne = new CharArrayReader(c, 0, 5)) {
            System.out.print("inputOne is: ");
            while ((i = inputOne.read()) != -1) {
                System.out.print((char)i);
            }
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //writer
        CharArrayWriter output = new CharArrayWriter();
        try {
            output.write(c);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Buffer as a string");
        System.out.println(output.toString());
        System.out.println("Into array");

        char[] ch = output.toCharArray();
        for (int j = 0; j < ch.length; j++) {
            System.out.print(ch[j]);
        }
        System.out.println("\nTo a FileWriter");

        try (FileWriter fileWriter = new FileWriter("test.txt")) {
            output.writeTo(fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Doing a reset");
        output.reset();

        for (int j = 0; j < 3; ++j) {
            output.write('X');
        }

        System.out.println(output.toString());
    }
}
