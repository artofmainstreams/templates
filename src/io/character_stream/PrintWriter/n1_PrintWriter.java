package io.character_stream.PrintWriter;

import java.io.PrintWriter;

/**
 * Created by frien on 25.07.2016.
 */
public class n1_PrintWriter {
    public static void main(String[] args) {
        /* Constructors
        - PrintWriter(OutputStream outputStream)
        - PrintWriter(OutputStream outputStream, boolean autoFlushingOn)
        - PrintWriter(Writer outputStream)
        - PrintWriter(Writer outputStream, boolean autoFlushingOn)
        - PrintWriter(File outputFile)
        - PrintWriter(File outputFile, String charSet)
        - PrintWriter(String outputFileName)
        - PrintWriter(String outputFileName, String charSet)
         */
         /* methods
        - print
        - println
        - PrintWriter printf(String fmtString, Object...args)
        - PrintWriter printf(Locale loc, String fmtString, Object...args)
        - PrintWriter format(String fmtString, Object...args)
        - PrintWriter format(Locale loc, String fmtString, Objects...args)
         */
        PrintWriter pw = new PrintWriter(System.out, true);
        pw.println("This is a string");
        int i = - 7;
        pw.println(i);
    }
}
