package io.character_stream.FileReader_and_FileWriter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by frien on 06.08.2016.
 */
public class n1_FileReader_and_FileWriter {
    /* Constructors FileReader
    - FileReader(String filePath)
    - FileReader(File fileObj)
     */
    /* Constructors FileWriter
    - FileWriter(String filePath)
    - FileWriter(String filePath, boolean append)           /- добавление в конец файла -/
    - FileWriter(File fileObj)
    - FileWriter(File fileObj, boolean append)
     */
    public static void main(String[] args) {
        String source = "This is the text";

        try (FileWriter fw = new FileWriter("File.txt")) {
            fw.write(source);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileReader fr = new FileReader("File.txt")) {
            int c;
            while ((c = fr.read()) != -1) {
                System.out.print((char) c);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
