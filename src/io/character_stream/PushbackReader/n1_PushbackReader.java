package io.character_stream.PushbackReader;

import java.io.CharArrayReader;
import java.io.IOException;
import java.io.PushbackReader;

/**
 * Created by frien on 06.08.2016.
 */
public class n1_PushbackReader {
    /* Constructors
    - PushbackReader(Reader inputStream)
    - PushbackReader(Reader inputStream, int bufSize)
     */
    /* methods
    - void unread(int ch)
    - void unread(char buffer[])
    - void unread(char bufferp[, int offer, int numChars)
     */
    public static void main(String[] args) {
        String s = "if (a == 4) a = 0;\n";
        char buf[] = new char[s.length()];
        s.getChars(0, s.length(), buf, 0);
        CharArrayReader in = new CharArrayReader(buf);
        int c;

        try (PushbackReader file = new PushbackReader(in)) {
            while ((c = file.read()) != -1) {
                switch(c) {
                    case '=':
                        if ((c = file.read()) == '=') {
                            System.out.print(".eq.");
                        } else {
                            System.out.print("<-");
                            file.unread(c);
                        }
                        break;
                    default:
                        System.out.print((char)c);
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
