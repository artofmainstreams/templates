package io.character_stream;

import java.io.*;

/**
 * Created by frien on 25.07.2016.
 */
public class n1 {
    /*************/
    Reader ob1;                 //Abstract class that describes character stream input
    Writer ob2;                 //Abstract class that describes character stream output
    //read()
    //write()
    /*************/
    BufferedReader ob3;         //Buffered input character stream
    BufferedWriter ob4;         //Buffered output character stream

    CharArrayReader ob5;        //Input stream that reads from a character array
    CharArrayWriter ob6;        //Output stream that writes to a character array

    FileReader ob7;             //Input stream that reads from a file
    FileWriter ob8;             //Output stream that writes to a file

    FilterReader ob9;           //Filtered reader
    FilterWriter ob10;          //Filtered writer

    InputStreamReader ob11;     //Input stream that translates bytes to characters
    OutputStreamWriter ob12;    //Output stream that translates characters to bytes

    LineNumberReader ob13;      //Input stream that counts lines

    PipedReader ob14;           //Input pipe
    PipedWriter ob15;           //Output pipe

    PrintWriter ob16;           //Output stream that contains print() and println()
    PushbackReader ob17;        //Input stream that allows characters to be returned to the input
                                //stream
    StringReader ob18;          //Input stream that reads from a string
    StringWriter ob19;          //Output stream that writes to a string
}