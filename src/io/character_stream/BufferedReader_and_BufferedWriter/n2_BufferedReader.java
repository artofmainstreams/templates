package io.character_stream.BufferedReader_and_BufferedWriter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by frien on 25.07.2016.
 */
public class n2_BufferedReader {
    public static void main(String[] args) throws IOException { //Reading Console Input
        //BufferedReader(Reader inputReader)
        //InputStreamReader(InputStream inputStream)
        /* System.in - InputReader, байтовый. Оборачиваем в InputSreamReader,
            переводящий байты в символы. Всё это заворачиваем в буфер.
         */
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //read
        char c;
        do {
            c = (char) br.read();
            System.out.println("Введённый символ: " + c);
        } while (c != 'q'); //конец потока -1
        //reading strings
        String str;
        do {
            str = br.readLine();
            System.out.println("Введённая строка: " + str);
        } while (!str.equals("stop"));
    }
}
