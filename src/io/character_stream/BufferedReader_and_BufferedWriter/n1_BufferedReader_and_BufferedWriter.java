package io.character_stream.BufferedReader_and_BufferedWriter;

import java.io.BufferedReader;
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.IOException;

/**
 * Created by frien on 06.08.2016.
 */
public class n1_BufferedReader_and_BufferedWriter {
    /* Constructors BufferedReader
    - BufferedReader(Reader inputStream)
    - BufferedReader(Reader inputStream, int bufSize)
     */
    /* Constructors BufferedWriter
    - BufferedWriter(Writer outputStream)
    - BufferedWriter(Writer outputStream, int bufSize)
     */
    /* methods BufferedReader
    - lines
     */
    public static void main(String[] args) {
        String s = "This is a &copy; copyringht symbol but this is &copy not\n";
        char[] buf = new char[s.length()];
        s.getChars(0, s.length(), buf, 0);

        CharArrayReader in = new CharArrayReader(buf);
        int c;
        boolean marked = false;

        try (BufferedReader file = new BufferedReader(in)) {
            while ((c = file.read()) != -1) {
                switch (c) {
                    case '&':   //ставим метку, ничего не печатаем
                        if (!marked) {
                            file.mark(32);
                            marked = true;
                        } else {
                            marked = false;
                        }
                        break;
                    case ';':   //снимаем метку, печатаем спецсимвол
                        if (marked) {
                            marked = false;
                            System.out.print("(c)");
                        } else {
                            System.out.print((char) c);
                        }
                        break;
                    case ' ':   //возвращаемся к метке, снимаем и печатаем дальше
                        if (marked) {
                            marked = false;
                            file.reset();
                            System.out.print("&");
                        } else {
                            System.out.print((char) c);
                        }
                        break;  //печатаем символ
                    default:
                        if (!marked) {
                            System.out.print((char) c);
                        }
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
