package io.character_stream;

import java.io.Console;

/**
 * Created by frien on 06.08.2016.
 */
public class n2_Console {
    /* конструкторы отсутствуют
    - static Console console()
     */
    /* methods
    - void flush()
    - Console format(String fmtString, Objects...args)
    - Console printf(Strinf fmtString, Objects...args)
    - Reader reader()                                       /- получить ссылку на подключённый Reader -/
    - String readLine()                                     /- возвращает null если достигнут конец -/
    - String readLine(String fmtString, Objects...args)
    - char[] readPassword()
    - char[] readPassword(String fmtString, Objects args)
    - PrintWriter writer()
     */
    public static void main(String[] args) {
        String str;
        Console console = System.console();     //получить ссылку на консоль
        if (console == null) {
            System.out.println("Консоль не найдена");
            return;
        }
        str = console.readLine("Enter a string: ");
        console.printf("Here is your string: %s\n", str);
        char[] password = console.readPassword("Enter a password: ");
        System.out.println("Your password: " + password);
    }
}
