package io.character_stream.Reader_and_Writer;

/**
 * Created by frien on 31.07.2016.
 */
public class Reader_and_Writer {
    /* Reader
    - abstract void close()
    - void mark(int numChars)
    - boolean markSupported()
    - int read()
    - int read(chat buffer[])
    - int read(CharBuffer buffer)
    - abstract int read(char buffer[], int offset, int numChars)
    - boolean ready()
    - void reset()
    - long skip(long numChars)
     */
    /* Writer
    - Writter append(char ch)
    - Writer append(CharSequence chars)
    - Writter append(CharSequence chars, int begin, int end)
    - abstract void close()
    - abstract void flush()
    - void write(int ch)
    - void write(char buffer[])
    - abstract void write(char buffer[], int offset, int numChars)
    - void write(String str)
    - void write(String str, int offset, int numChars)
     */
}
