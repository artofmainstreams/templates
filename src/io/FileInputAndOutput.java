package io;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Scanner;

/*
Запись в файл и чтение из него. Простая универсальная реализация.
 */

public class FileInputAndOutput {
    public static void main(String[] args) throws IOException {
        PrintWriter out = new PrintWriter("myfile.txt", "UTF-8");
            out.println("Это строка");
            out.print(7);
            out.flush();
        //кодировку обязательно, иначе он не воспримет это как файл
        Scanner in = new Scanner(Paths.get("myfile.txt"), "UTF-8");
            System.out.println(in.nextLine());
            System.out.print(in.nextInt());
    }
}
