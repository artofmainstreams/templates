package io.file.directory;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

/**
 * Created by frien on 30.07.2016.
 */
public class n1 {
    /* Directory
    - String[] list()                           /- отобразить все файлы этой директории -/
    - String[] list(FilenameFilter FFObj)       /- отобразить все файлы, кроме некоторых названий -/
    - File[] listFiles()
    - File[] listFiles(FilenameFilter FFObj)
    - File[] listFiles(FileFilter FObj)         /- отобразить все файлы, кроме некоторых путей -/
    - mkdir
    - mkdirs
     */
    /* interface FilenameFilter
    - boolean accept(File directory, String filename)
     */
    /* interface FileFilter
    - boolean accept(File path)
     */
    /* interface AutoClosable
    - void close() throws Exception
     */
    /* interface Closable extends AutoClosable
    - void close() throws Exception
     */
    /* interface Flushable
    - void flush() throws IOException
     */
    public static void main(String[] args) {
        File f1 = new File("/");
        FilenameFilter only = new OnlyExt("dll");
        System.out.println(Arrays.toString(f1.list(only)));
    }
}

class OnlyExt implements FilenameFilter {
    String ext;

    public OnlyExt(String ext) {
        this.ext = "." + ext;
    }

    @Override
    public boolean accept(File dir, String name) {
        return name.endsWith(ext);
    }
}