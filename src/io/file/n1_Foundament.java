package io.file;

import java.io.File;

/**
 * Created by frien on 30.07.2016.
 */
public class n1_Foundament {
    /* Constructors
    - File(String directoryPath)
    - File(String directoryPath, String fileName)
    - File(File dirObj, String filename)
    - File(URI uriObj)
     */
    /* methods
    - getName
    - getPath
    - getAbsolutePath
    - getParent
    - boolean exists()
    - boolean canWrite()
    - boolean canRead()
    - boolean isDirectory()
    - boolean isFile()
    - boolean isAbsolute()
    - long lastModified
    - long length()
    - boolean renameTo(File newName)            /- переименовать файл или директорию -/
    - boolean delete()                          /- удалить пустую директорию или удалить файл -/
    */
    public static void main(String[] args) {
        File f1 = new File("/");
        File f2 = new File("/", "autoexec.bat");
        File f3 = new File(f1, "autoexec.bat");

        File f4 = new File("src\\foundament\\string\\StringBuilder", "_1_StringBuilder.java");
        show("File name: " + f4.getName());
        show("Path: " + f4.getPath());
        show("Abs Path: " + f4.getAbsolutePath());
        show("Parent: " + f4.getParent());
        show(f4.exists() ? "exists" : "does not exists");
        show(f4.canWrite() ? "is writeable" : "is not writeable");
        show(f4.canRead() ? "is readable" : "is not readable");
        show("is " + (f4.isDirectory() ? "" : "not") + " a directory");
        show(f4.isFile() ? "is normal file" : "might be a named pipe");
        show(f4.isAbsolute() ? "is absolute" : "is not absolute");
        show("File last modified: " + f4.lastModified());
        show("File size: " + f4.length() + " Bytes");

    }

    static void show(String s) {
        System.out.println(s);
    }
}

